var jsHelper = function(){};

jsHelper.prototype.includeJsScripts = function (scripts) {

    scripts.forEach(function (attributes, key) {
        var js = document.createElement("script");

        Object.keys(attributes).forEach(function (key) {
            js.setAttribute(key, attributes[key]);
        });

        document.body.appendChild(js);
    });
}