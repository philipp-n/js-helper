# ◦ Requirements

- Vanilla JS

# ◦ Install

### Via hosting server
Latest
```html
<script src="https://philippn.com/vendor/js-helper/dist.js"></script>
```
Certain version
```html
<script src="https://philippn.com/vendor/js-helper/0.0.2/dist.js"></script>
```

# ◦ Use

```html
<script>
    window.onload = function(){
        new jsHelper().includeJsScripts([
            {
                src: "https://philippn.com/vendor/auto-reload/ajax-wrapper.js",
                url: "/tellimiskeskus/varuoasa_otsing/results/manufacturer=74/model=5463/carid=19056/cartype=P/vinnumber=WDC2510221A009670/regnumber=086BSX"
            }
        ]);
    }
</script>
```